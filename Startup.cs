using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using taxmanager.api.AppConfig;
using taxmanager.api.Business.Repository;
using taxmanager.api.Business.Service;
using taxmanager.api.DataAccess;
using taxmanager.api.Security;

namespace taxmanager.api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private ServicesConfig _servicesConfig;
        private RepositoriesConfig _repositoriesConfig;
        private AuthConfig _authConfig;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region DatabaseConfig
            services.AddTransient<IDbConnection>(opt => new MySqlConnection(Configuration.GetConnectionString("MySQLConnection")));
            #endregion

            #region Authentication
            _authConfig = new AuthConfig(ref services, Configuration);
            _authConfig.AddAuthOnStartup();
            #endregion

            #region Repositories
            _repositoriesConfig = new RepositoriesConfig(ref services);
            _repositoriesConfig.AddRepositoriesOnStartup();
            #endregion

            #region Services
            _servicesConfig = new ServicesConfig(ref services);
            _servicesConfig.AddServicesOnStartup();
            #endregion

            services.AddControllers();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
