﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace taxmanager.api.Security
{
    public class TokenRequest
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
