﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using taxmanager.api.Business.Service;

namespace taxmanager.api.AppConfig
{
    public class ServicesConfig
    {
        protected IServiceCollection _services;
        public ServicesConfig(ref IServiceCollection services)
        {
            _services = services;
        }

        public void AddServicesOnStartup()
        {
            _services.AddSingleton(typeof(GenericService<>));
            _services.AddScoped<UserService>();
        }
    }
}
