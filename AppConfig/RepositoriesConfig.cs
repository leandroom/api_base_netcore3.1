﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using taxmanager.api.Business.Repository;
using taxmanager.api.Business.Service;
using taxmanager.api.DataAccess;

namespace taxmanager.api.AppConfig
{
    public class RepositoriesConfig
    {
        protected IServiceCollection _services;
        public RepositoriesConfig(ref IServiceCollection services)
        {
            _services = services;
        }

        public void AddRepositoriesOnStartup()
        {
            _services.AddSingleton(typeof(IRepository<>), typeof(GenericRepository<>));
            _services.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
