﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using taxmanager.api.Business.Model;
using taxmanager.api.Business.Service;

namespace taxmanager.api.Controllers
{
    [Authorize()]
    public abstract class GenericController<T> : ControllerBase where T: class
    {
        protected GenericService<T> _service;

        public GenericController(GenericService<T> service)
        {
            this._service = service;
        }

        [HttpGet]
        public virtual IEnumerable<T> GetAll()
        {
            return _service.GetAll();
        }

        [HttpGet("{id}")]
        public virtual T Get( int id)
        {
            return _service.Get(id);
        }

        [HttpPost]
        public void Insert([FromBody] T entity)
        {
            _service.Insert(entity);
        }

        [HttpPut]
        public void Update([FromBody]T entity)
        {
            _service.Update(entity);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}