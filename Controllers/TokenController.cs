﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using taxmanager.api.Business.Model;
using taxmanager.api.Business.Service;
using taxmanager.api.Security;

namespace taxmanager.api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly TokenConfiguration _tokenConfiguration;
        private readonly UserService _userService;

        public TokenController(IConfiguration configuration,
                                TokenConfiguration tokenConfiguration,
                                UserService userService)
        {
            _configuration = configuration;
            _tokenConfiguration = tokenConfiguration;
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("GET OK");
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult RequestToken([FromBody] TokenRequest request)
        {
            if (request == null || String.IsNullOrEmpty(request.User) || String.IsNullOrEmpty(request.Password))
            {
                return BadRequest("Credenciais inválidas");
            }

            var user = _userService.FindByLoginAndPass(request.User, request.Password);

            if (user != null)
            {
                return GenerateToken(user);
            }

            return Unauthorized();
        }

        private IActionResult GenerateToken(User user)
        {
            var claims = new[]
                {
                    new Claim("userLogin", user.Login),
                    new Claim("userEmail", user.Email),
                    new Claim("userName", user.Name)
                };


            var key = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _tokenConfiguration.Issuer,
                    audience: _tokenConfiguration.Audience,
                    claims: claims,
                    expires: DateTime.Now.AddSeconds(_tokenConfiguration.Seconds),
                    signingCredentials: creds
                );

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token)
            });
        }
    }
}
