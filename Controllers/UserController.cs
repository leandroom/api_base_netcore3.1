﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using taxmanager.api.Business.Model;
using taxmanager.api.Business.Service;

namespace taxmanager.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize()]
    public class UserController : GenericController<User>
    {
        public UserController(UserService userService) : base(userService)
        {
        }
    }
}