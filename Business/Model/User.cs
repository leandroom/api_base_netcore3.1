﻿using Dapper.Contrib.Extensions;

namespace taxmanager.api.Business.Model
{
    [Table("User")]
    public class User : BaseModel
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
