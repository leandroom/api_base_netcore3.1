﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using taxmanager.api.Business.Model;

namespace taxmanager.api.Business.Repository
{
    public class GenericRepository<TModel> : IRepository<TModel>, IDisposable where TModel : class
    {
        protected readonly IDbConnection _db;

        public GenericRepository(IDbConnection dbConnection)
        {
            _db = dbConnection;
        }

        public IEnumerable<TModel> GetAll()
        {
            return _db.GetAll<TModel>();
        }

        public virtual TModel Get(int id)
        {
            return _db.Get<TModel>(id);
        }

        public void Insert(TModel entity)
        {
            entity.GetType().GetProperty("CreatedAt").SetValue(entity, DateTime.Now);
            entity.GetType().GetProperty("CreatedBy").SetValue(entity, 1);
            _db.Insert<TModel>(entity);
        }

        public void Update(TModel entity)
        {
            entity.GetType().GetProperty("UpdatedAt").SetValue(entity, DateTime.Now);
            entity.GetType().GetProperty("UpdatedBy").SetValue(entity, 1);
            _db.Update<TModel>(entity);
        }

        public void Delete(TModel entity)
        {
            _db.Delete<TModel>(entity);
        }

        public void Dispose()
        {
            if (_db != null) 
                _db.Dispose();
        }
    }
}
