﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace taxmanager.api.Business.Repository
{
    public interface IRepository<TModel> : IDisposable where TModel: class
    {
        IEnumerable<TModel> GetAll();
        TModel Get(int id);
        void Insert(TModel entity);
        void Update(TModel entity);
        void Delete(TModel entity);

    }
}
