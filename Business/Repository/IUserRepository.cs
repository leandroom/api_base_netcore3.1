﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using taxmanager.api.Business.Model;

namespace taxmanager.api.Business.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        User FindByLoginAndPass(string login, string pass);
    }
}
