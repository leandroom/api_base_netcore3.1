﻿using System.Collections.Generic;
using taxmanager.api.Business.Repository;

namespace taxmanager.api.Business.Service
{
    public class GenericService<TModel> where TModel: class
    {
        protected IRepository<TModel> _repository;

        public GenericService(IRepository<TModel> repository)
        {
            _repository = repository;
        }

        public IEnumerable<TModel> GetAll()
        {
            return _repository.GetAll();
        }

        public TModel Get(int id)
        {
            return _repository.Get(id);
        }

        public void Insert(TModel entity)
        {
            _repository.Insert(entity);
        }

        public void Update(TModel entity)
        {
            _repository.Update(entity);
        }

        public void Delete(int id)
        {
            TModel model = _repository.Get(id);
            if (model != null)
                _repository.Delete(model);
        }
    }
}
