﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using taxmanager.api.Business.Model;
using taxmanager.api.Business.Repository;

namespace taxmanager.api.Business.Service
{
    public class UserService : GenericService<User>
    {
        private readonly IUserRepository _repoUser;

        public UserService(IUserRepository repository) : base(repository)
        {
            this._repoUser = repository;
        }

        public User FindByLoginAndPass(string login, string pass)
        {
            var hashPass = CalculateHash(login + pass);
            var user = _repoUser.FindByLoginAndPass(login, hashPass);
            return user;
        }

        //Gerar o hash code de senha
        private string CalculateHash(string str)
        {
            using (var md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(str));

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("x2"));
                }

                return sb.ToString();
            }
        }
    }
}
