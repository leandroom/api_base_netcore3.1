﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using taxmanager.api.Business.Model;
using taxmanager.api.Business.Repository;

namespace taxmanager.api.DataAccess
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(IDbConnection dbConnection) : base(dbConnection)
        {
        }

        public User FindByLoginAndPass(string login, string pass)
        {
            var user = _db.QueryFirstOrDefault<User>("select * from User where Login = @Login and Password = @Password", new { Login = login, Password = pass });
            return user;
        }
    }
}
